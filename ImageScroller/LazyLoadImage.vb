﻿Imports Windows.Storage

Public Class LazyLoadImage

    Public Sub New(image As StorageFile)
        BaseImage = image
    End Sub

#Region "Utilities"

    Private Async Function LoadBitmapAsync(bitmap As BitmapImage) As Task
        ' Debug.WriteLine("Loading file: {0}", BaseImage.Name)
        bitmap.CreateOptions = BitmapCreateOptions.None
        Dim stream = Await BaseImage.OpenReadAsync()
        Await bitmap.SetSourceAsync(stream)
    End Function

#End Region

#Region "Properties"

    Private _WeakBitmap As WeakReference(Of BitmapImage)

    Public ReadOnly Property Bitmap As BitmapImage
        Get
            Return GetBitmapAsync().Result
        End Get
    End Property

    Public Async Function GetBitmapAsync() As Task(Of BitmapImage)
        Dim ret As BitmapImage = Nothing
        If _WeakBitmap IsNot Nothing Then
            If _WeakBitmap.TryGetTarget(ret) Then
                Return ret
            End If
        End If
        ret = New BitmapImage()
        Await LoadBitmapAsync(ret)
        If _WeakBitmap IsNot Nothing Then
            _WeakBitmap.SetTarget(ret)
        Else
            _WeakBitmap = New WeakReference(Of BitmapImage)(ret)
        End If
        Return ret
    End Function

    Private _BaseImage As StorageFile

    Public Property BaseImage As StorageFile
        Get
            Return _BaseImage
        End Get
        Private Set(value As StorageFile)
            _BaseImage = value
        End Set
    End Property

#End Region

End Class
