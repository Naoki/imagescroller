﻿Imports System.Runtime.Serialization
Imports System.Xml.Serialization
Imports Windows.Storage
Imports Windows.Storage.FileProperties
Imports Windows.Storage.Search

<DataContract>
Public Class FolderItem
    Implements INotifyPropertyChanged

    Public Sub New()

    End Sub

    <DataMember>
    Public Property Title As String

    Private _Thumbnail As ImageSource

    <XmlIgnore, IgnoreDataMember>
    Public Property Thumbnail As ImageSource
        Get
            Return _Thumbnail
        End Get
        Set(value As ImageSource)
            If _Thumbnail IsNot value Then
                _Thumbnail = value
                OnPropertyChanged(New PropertyChangedEventArgs("Thumbnail"))
            End If
        End Set
    End Property

    <XmlIgnore, IgnoreDataMember>
    Public Property Folder As StorageFolder

    <DataMember>
    Public Property FolderPath As String
        Get
            If Folder Is Nothing Then Return Nothing
            Return Folder.Path
        End Get
        Set(value As String)
            If value Is Nothing Then
                Folder = Nothing
            Else
                Folder = StorageFolder.GetFolderFromPathAsync(value).AsTask().Result
            End If
        End Set
    End Property

    Public Async Function LoadThumbnailAsync() As Task
        Dim source As New BitmapImage()
        Dim filesInFolder = From file In Await Folder.GetFilesAsync(CommonFileQuery.OrderByName) Order By file.Name Select file
        For Each file In filesInFolder
            If file.ContentType.StartsWith("image") Then
                Await source.SetSourceAsync(Await file.GetScaledImageAsThumbnailAsync(ThumbnailMode.SingleItem, 250, ThumbnailOptions.UseCurrentScale))
                Thumbnail = source
                Return
            End If
        Next
        Await source.SetSourceAsync(Await Folder.GetThumbnailAsync(ThumbnailMode.SingleItem))
        Thumbnail = source
    End Function

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged

    Protected Overridable Sub OnPropertyChanged(e As PropertyChangedEventArgs)
        RaiseEvent PropertyChanged(Me, e)
    End Sub

    <XmlIgnore, IgnoreDataMember>
    Public Overridable ReadOnly Property ShowSpecialSymbol As Boolean
        Get
            Return False
        End Get
    End Property

    <DataMember>
    Public Property ScrollState As ScrollState

    Private _ToolTip As String

    <DataMember>
    Public Property ToolTip As String
        Get
            If _ToolTip Is Nothing AndAlso Folder IsNot Nothing Then
                _ToolTip = Folder.Path
            End If
            Return _ToolTip
        End Get
        Set(value As String)
            _ToolTip = value
        End Set
    End Property

    <DataMember>
    Public Property PageIndex As Integer

End Class
