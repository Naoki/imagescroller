﻿
Public Class BooleanToVisibilityConverter
    Implements IValueConverter

    Public Function Convert(value As Object, targetType As Type, parameter As Object, language As String) As Object Implements IValueConverter.Convert
        If value IsNot Nothing AndAlso TypeOf value Is Boolean AndAlso CBool(value) Then
            Return Visibility.Visible
        End If
        Return Visibility.Collapsed
    End Function

    Public Function ConvertBack(value As Object, targetType As Type, parameter As Object, language As String) As Object Implements IValueConverter.ConvertBack
        Return CInt(value) = Visibility.Visible
    End Function

End Class
