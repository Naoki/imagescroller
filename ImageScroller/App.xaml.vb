﻿Imports ImageScroller.Common
Imports System.Xml.Serialization
Imports System.Xml
Imports System.Runtime.Serialization
Imports System.Text

' The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

''' <summary>
''' Provides application-specific behavior to supplement the default Application class.
''' </summary>
NotInheritable Class App
    Inherits Application

    Private folderItemsTaskCompletionSource As New TaskCompletionSource(Of ObservableCollection(Of FolderItem))

    Public Sub New()
        FolderItems = folderItemsTaskCompletionSource.Task

        InitializeComponent()
    End Sub

    ''' <summary>
    ''' Invoked when the application is launched normally by the end user.  Other entry points
    ''' will be used when the application is launched to open a specific file, to display
    ''' search results, and so forth.
    ''' </summary>
    ''' <param name="e">Details about the launch request and process.</param>
    Protected Overrides Async Sub OnLaunched(e As Windows.ApplicationModel.Activation.LaunchActivatedEventArgs)
#If DEBUG Then
        ' Show graphics profiling information while debugging.
        If System.Diagnostics.Debugger.IsAttached Then
            ' Display the current frame rate counters
            'Me.DebugSettings.EnableFrameRateCounter = True
        End If
#End If

        Dim rootFrame As Frame = TryCast(Window.Current.Content, Frame)

        ' Do not repeat app initialization when the Window already has content,
        ' just ensure that the window is active

        If rootFrame Is Nothing Then
            ' Create a Frame to act as the navigation context and navigate to the first page
            rootFrame = New Frame()

            SuspensionManager.RegisterFrame(rootFrame, "rootFrame")

            ' Set the default language
            rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages(0)

            AddHandler rootFrame.NavigationFailed, AddressOf OnNavigationFailed

            Await SuspensionManager.RestoreAsync()

            ' Place the frame in the current Window
            Window.Current.Content = rootFrame
        Else
            SuspensionManager.RegisterFrame(rootFrame, "rootFrame")
            Await SuspensionManager.RestoreAsync()
        End If

        LoadFolderItems()

        If rootFrame.Content Is Nothing Then
            ' When the navigation stack isn't restored navigate to the first page,
            ' configuring the new page by passing required information as a navigation
            ' parameter
            rootFrame.Navigate(GetType(TitlePage), e.Arguments)
        End If

        ' Ensure the current window is active
        Window.Current.Activate()
    End Sub

    Public Sub LoadFolderItems()
        Dim items As FolderItem()
        Dim serializer As New DataContractSerializer(GetType(FolderItem()))
        Try
            Dim save As String = CStr(SuspensionManager.SessionState("Recent"))
            Using reader As New StringReader(save)
                Using xReader As XmlReader = XmlReader.Create(reader)
                    items = DirectCast(serializer.ReadObject(xReader), FolderItem())
                End Using
            End Using
        Catch ex As Exception
            items = {}
        End Try

        folderItemsTaskCompletionSource.SetResult(New ObservableCollection(Of FolderItem)(items))
    End Sub

    Public Async Function SaveFolderItemsAsync() As Task
        Dim items As FolderItem() = (From x In Await FolderItems
                                     Where Not TypeOf x Is SpecialFolderItem
                                     Select x).ToArray()
        Dim serializer As New DataContractSerializer(GetType(FolderItem()))
        Dim save As New StringBuilder()
        Using xWriter As XmlWriter = XmlWriter.Create(save)
            serializer.WriteObject(xWriter, items)
        End Using
        SuspensionManager.SessionState("Recent") = save.ToString()
    End Function

    ''' <summary>
    ''' Invoked when Navigation to a certain page fails
    ''' </summary>
    ''' <param name="sender">The Frame which failed navigation</param>
    ''' <param name="e">Details about the navigation failure</param>
    Private Sub OnNavigationFailed(sender As Object, e As NavigationFailedEventArgs)
        Throw New Exception("Failed to load Page " + e.SourcePageType.FullName)
    End Sub

    ''' <summary>
    ''' Invoked when application execution is being suspended.  Application state is saved
    ''' without knowing whether the application will be terminated or resumed with the contents
    ''' of memory still intact.
    ''' </summary>
    ''' <param name="sender">The source of the suspend request.</param>
    ''' <param name="e">Details about the suspend request.</param>
    Private Async Sub OnSuspending(sender As Object, e As SuspendingEventArgs) Handles Me.Suspending
        Dim deferral As SuspendingDeferral = e.SuspendingOperation.GetDeferral()
        ' TODO: Save application state and stop any background activity
        Await SaveFolderItemsAsync()
        Await SuspensionManager.SaveAsync()
        deferral.Complete()
    End Sub

    Public Property FolderItems As Task(Of ObservableCollection(Of FolderItem))

    Public Shared Shadows ReadOnly Property Current As App
        Get
            Return DirectCast(Application.Current, App)
        End Get
    End Property

End Class
