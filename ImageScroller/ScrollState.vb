﻿Imports System.Runtime.Serialization

<DataContract>
Public Class ScrollState

    <DataMember>
    Public Property HorizontalOffset As Double

    <DataMember>
    Public Property VerticalOffset As Double

    <DataMember>
    Public Property ZoomFactor As Nullable(Of Double)

End Class
