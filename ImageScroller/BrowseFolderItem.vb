﻿
Public Class SpecialFolderItem
    Inherits FolderItem
    Implements IEquatable(Of SpecialFolderItem)

    Public Sub New()
        Title = "Browse folder..."
    End Sub

    Public Property SpecialSymbol As Symbol

    Public Overrides ReadOnly Property ShowSpecialSymbol As Boolean
        Get
            Return True
        End Get
    End Property

    Private Function Equals1(other As SpecialFolderItem) As Boolean Implements IEquatable(Of SpecialFolderItem).Equals
        Return SpecialSymbol = other.SpecialSymbol AndAlso Title = other.Title
    End Function

End Class
