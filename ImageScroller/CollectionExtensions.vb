﻿Public Module CollectionExtensions

    <Extension>
    Public Function GetValueOrDefault(Of TKey, TValue)(dictionary As IDictionary(Of TKey, TValue), key As TKey) As TValue
        Try
            Return dictionary(key)
        Catch ex As KeyNotFoundException
            Return Nothing
        End Try
    End Function

    <Extension>
    Public Function GetOrAdd(Of TKey, TValue)(dictionary As IDictionary(Of TKey, TValue), key As TKey, factory As Func(Of TValue)) As TValue
        Try
            Return dictionary(key)
        Catch ex As KeyNotFoundException
            Dim value As TValue = factory()
            dictionary(key) = value
            Return value
        End Try
    End Function

End Module
