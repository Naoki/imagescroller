﻿Imports System.IO
Imports System.IO.Compression
Imports System.IO.Packaging
Imports System.Threading
Imports System.Windows.Shell
Imports System.Windows.Xps.Packaging
Imports Microsoft.WindowsAPICodePack.Dialogs

Class MainWindow

    Private Sub folderToXpsButton_Click(sender As Object, e As RoutedEventArgs)
        Dim folderDialog As New CommonOpenFileDialog("Open folder") With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
            .IsFolderPicker = True,
            .EnsurePathExists = True,
            .EnsureFileExists = True,
            .ShowPlacesList = True
        }

        If folderDialog.ShowDialog(Me) <> CommonFileDialogResult.Ok Then Return
        Dim folder = folderDialog.FileName

        Dim saveDialog As New CommonSaveFileDialog("Save to XPS") With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
            .ShowPlacesList = True,
            .DefaultExtension = ".xps"}
        saveDialog.Filters.Add(New CommonFileDialogFilter("XPS Document", ".xps"))
        If saveDialog.ShowDialog(Me) <> CommonFileDialogResult.Ok Then Return
        Dim file = saveDialog.FileName

        Progress(0)
        ThreadPool.QueueUserWorkItem(Async Sub(t)
                                         If Await FolderToXpsAsync(folder, file, New Progress(Of Double)(AddressOf Progress)) Then
                                             Process.Start(file)
                                         End If
                                     End Sub)
    End Sub

    Private Async Sub zipFileToXpsButton_Click(sender As Object, e As RoutedEventArgs) Handles zipFileToXpsButton.Click
        Dim openDialog As New CommonOpenFileDialog("Open file") With {
            .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures),
            .EnsurePathExists = True,
            .ShowPlacesList = True
        }
        openDialog.Filters.Add(New CommonFileDialogFilter("Compressed (zipped) Folder", ".zip"))

        If openDialog.ShowDialog(Me) <> CommonFileDialogResult.Ok Then Return

        Dim saveDialog As New CommonSaveFileDialog("Save to XPS") With {
                .InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),
                .ShowPlacesList = True,
                .DefaultExtension = ".xps"}
        saveDialog.Filters.Add(New CommonFileDialogFilter("XPS Document", ".xps"))
        If saveDialog.ShowDialog(Me) <> CommonFileDialogResult.Ok Then Return
        Dim xpsFile = saveDialog.FileName

        Dim zipFile = openDialog.FileName
        Dim folder As String
        Do
            folder = Path.Combine(
                Path.GetTempPath,
                "ImageScroller-" & Path.GetRandomFileName())
        Loop While Directory.Exists(folder)
        Directory.CreateDirectory(folder)
        Using fileStream = File.OpenRead(zipFile)
            Dim zipStream As New ZipArchive(fileStream)
            Dim entriesCount = zipStream.Entries.Count
            Dim entriesIndex = 0
            Progress(0)
            For Each entry In zipStream.Entries
                Dim entryDirectories As String() = entry.FullName.Split("/")
                Dim fullDestinationDirectoryList As New List(Of String) From {folder}
                fullDestinationDirectoryList.AddRange(entryDirectories.Take(entryDirectories.Count - 1))
                Dim fullDestinationDirectory = Path.Combine(fullDestinationDirectoryList.ToArray())
                Directory.CreateDirectory(fullDestinationDirectory)
                If String.IsNullOrEmpty(entryDirectories(entryDirectories.Count - 1)) Then
                    Continue For
                End If
                Using outStream = File.Create(Path.Combine(fullDestinationDirectory, entry.Name))
                    Using entryStream = entry.Open()
                        Await entryStream.CopyToAsync(outStream)
                    End Using
                End Using

                entriesIndex += 1
                Progress(entriesIndex / entriesCount)
            Next
        End Using

        Progress(0)
        ThreadPool.QueueUserWorkItem(Async Sub(t)
                                         If Await FolderToXpsAsync(folder, xpsFile, New Progress(Of Double)(AddressOf Progress)) Then
                                             Process.Start(xpsFile)
                                         End If
                                     End Sub)
    End Sub

    Private Sub Progress(value As Double)
        If Dispatcher.Thread.ManagedThreadId <> Thread.CurrentThread.ManagedThreadId Then
            Dispatcher.Invoke(Sub() Progress(value))
            Return
        End If

        Dim isInf = value = Double.PositiveInfinity
        progressBar.IsIndeterminate = isInf
        If isInf Then
            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Indeterminate
        Else
            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.Normal
            progressBar.Value = value * 100
            TaskbarItemInfo.ProgressValue = value
        End If
        If value = 1 Then
            TaskbarItemInfo.ProgressState = TaskbarItemProgressState.None
        End If
    End Sub

    Private Async Function FolderToXpsAsync(folder As String, xpsFile As String, progress As IProgress(Of Double)) As Task(Of Boolean)
        Dim commonImageExtensions = {".jpg", ".jpeg", ".png", ".gif"}
        Dim files = From f In GetAncestorFiles(folder)
                    Let info As FileInfo = New FileInfo(f)
                    From ext In commonImageExtensions
                    Where info.Extension = ext
                    Select info

        If Not files.Any() Then
            Dispatcher.Invoke(Sub() MessageBox.Show("No images found in folder."))
            Return False
        End If

        Try
            Await ImagesToXpsAsync(files.ToList(), xpsFile, progress)
            Return True
#If DEBUG Then
        Catch ex As Exception
            Debugger.Break()
#Else
        Catch ex As Exception
            MessageBox.Show(ex.Message)
#End If
        End Try
        Return False
    End Function

    Private Iterator Function GetAncestorFiles(d As String) As IEnumerable(Of String)
        For Each subdir In Directory.GetDirectories(d).OrderBy(Function(dd) New DirectoryInfo(d).Name)
            For Each f In GetAncestorFiles(subdir)
                Yield f
            Next
        Next
        For Each file In Directory.GetFiles(d).OrderBy(Function(f) New FileInfo(f).Name)
            Yield file
        Next
    End Function

    Private Async Function ImagesToXpsAsync(files As IList(Of FileInfo), xpsFile As String, progress As IProgress(Of Double)) As Task
        progress.Report(Double.PositiveInfinity)
        Dim filesCount = files.Count
        If filesCount = 0 Then
            Throw New ArgumentException("images")
        End If

        If File.Exists(xpsFile) Then
            File.Delete(xpsFile)
        End If

        Using p = Package.Open(xpsFile)
            Dim document As New XpsDocument(p, CompressionOption.Normal)

            ' Generate thumbnail
            Dim firstFile As FileInfo = files.FirstOrDefault()
            If firstFile IsNot Nothing Then
                Dim thumbnail As XpsThumbnail = document.AddThumbnail(XpsImageType.PngImageType)
                document.Thumbnail = thumbnail
                Do
                    Using thumbnailStream = thumbnail.GetStream()

                        ' Read the image.
                        Dim image As BitmapImage
                        image = New BitmapImage()
                        image.BeginInit()
                        image.DecodePixelWidth = 255
                        image.StreamSource = firstFile.OpenRead()
                        image.CreateOptions = BitmapCreateOptions.None
                        image.EndInit()

                        ' Save as a thumbnail.
                        Dim encoder As New PngBitmapEncoder() With {.Interlace = PngInterlaceOption.On}
                        encoder.Frames.Add(BitmapFrame.Create(image))
                        encoder.Save(thumbnailStream)
                    End Using
                Loop While False
                thumbnail.Commit()
            End If

            Dim xpsWriter = document.AddFixedDocumentSequence()
            Dim fixedDocumentWriter = xpsWriter.AddFixedDocument()

            ' Iterate through files.
            Dim fileIndex = 0
            For Each file In files
                ' Read the image
                Dim image As New BitmapImage()
                image.BeginInit()
                image.StreamSource = file.OpenRead()
                image.CreateOptions = BitmapCreateOptions.None
                image.EndInit()

                ' Create a page.
                Dim pageWriter = fixedDocumentWriter.AddFixedPage()

                ' Add the image to the page's resources.
                Dim pageImage = pageWriter.AddImage(XpsImageType.PngImageType)
                Using pageImageStream = pageImage.GetStream()
                    Dim encoder As New PngBitmapEncoder() With {.Interlace = PngInterlaceOption.On}
                    encoder.Frames.Add(BitmapFrame.Create(image))
                    encoder.Save(pageImageStream)
                End Using
                pageImage.Commit()

                ' Create page XML.
                Dim w = image.PixelWidth
                Dim h = image.PixelHeight
                Dim pageXml = <?xml version="1.0" encoding="utf-8"?>
                              <FixedPage
                                  xmlns="http://schemas.microsoft.com/xps/2005/06"
                                  Width=<%= w %>
                                  Height=<%= h %>
                                  xml:lang="en-US">
                                  <Path Data=<%= String.Format("M 0,0 L {0},0 {0},{1} 0,{1} Z", w, h) %>>
                                      <Path.Fill>
                                          <ImageBrush
                                              ImageSource=<%= pageImage.Uri %>
                                              Viewbox=<%= String.Format("0,0,{0:0},{1:0}", w / image.DpiX * 96, h / image.DpiY * 96) %>
                                              Viewport=<%= String.Format("0,0,{0:0},{1:0}", w, h) %>
                                              ViewboxUnits="Absolute"
                                              ViewportUnits="Absolute"
                                              TileMode="Tile"/>
                                      </Path.Fill>
                                  </Path>
                              </FixedPage>
                pageXml.WriteTo(pageWriter.XmlWriter)

                pageWriter.Commit()

                fileIndex += 1
                progress.Report(fileIndex / filesCount)
                Await Dispatcher.InvokeAsync(AddressOf Noop)
            Next

            fixedDocumentWriter.Commit()
            xpsWriter.Commit()
            p.Flush()
        End Using
    End Function

    Private Sub Noop()
    End Sub

End Class
