﻿Imports System.IO

Public Class LazyLoadImage

    Public Sub New(image As FileInfo)
        BaseImage = image
    End Sub

#Region "Utilities"

    Private Async Function LoadBitmapAsync(bitmap As BitmapImage) As Task
        Debug.WriteLine("Loading file: {0}", BaseImage.Name)
        bitmap.BeginInit()
        bitmap.CreateOptions = BitmapCreateOptions.None
        Dim stream As New FileStream(BaseImage.FullName, FileMode.Open)
        bitmap.StreamSource = stream
        bitmap.EndInit()
    End Function

#End Region

#Region "Properties"

    Private _WeakBitmap As WeakReference(Of BitmapImage)

    Public ReadOnly Property Bitmap As BitmapImage
        Get
            Dim ret As BitmapImage
            If _WeakBitmap IsNot Nothing Then
                If _WeakBitmap.TryGetTarget(ret) Then
                    Return ret
                End If
            End If
            ret = New BitmapImage()
            LoadBitmapAsync(ret)
            If _WeakBitmap IsNot Nothing Then
                _WeakBitmap.SetTarget(ret)
            Else
                _WeakBitmap = New WeakReference(Of BitmapImage)(ret)
            End If
            Return ret
        End Get
    End Property

    Private _BaseImage As FileInfo

    Public Property BaseImage As FileInfo
        Get
            Return _BaseImage
        End Get
        Private Set(value As FileInfo)
            _BaseImage = value
        End Set
    End Property

#End Region

End Class
